using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    //Adjustable variable for sensitivity of mouse
    public float mouseSensitivity = 100f;

    //Refernce to player object
    public Transform playerBody;

    
    private float xRotation = 0f;

    private void Start()
    {
        //Lock mouse in the middle
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        //Set Axis of mouse to variable 
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        //Transform to movement 
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
