using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LoadNextlevel : MonoBehaviour
{
    //refernce to scene 
    [SerializeField] private string sceneNameToLoad;

    //Load Scene 
    public void LoadNextScene()
    {
        SceneManager.LoadScene(sceneNameToLoad); 
    }
}
