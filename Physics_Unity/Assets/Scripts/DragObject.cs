using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    //Vraibales for mouse dragging and dropping 
    private Vector3 mOffset;
    private float mZCoord;

    //variable for checking if block is on tower of blocks 
    public bool isOnTower;

    //Check mouse click
    private void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z; 
        mOffset = gameObject.transform.position - GetMouseWorldPos(); 
    }

    //Check dragging of mouse 
    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + mOffset; 
    }

    //Get Vector of mouse 
    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;

        mousePoint.z = mZCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint); 
    }

    //Check if object is standing on tower of blocks 
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "BuildingBlock")
        {
            isOnTower = true;
        }
    }

    //Check if object is not standing on tower of blocks anymore 
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "BuildingBlock")
        {
            isOnTower = false;
        }
    }
}
