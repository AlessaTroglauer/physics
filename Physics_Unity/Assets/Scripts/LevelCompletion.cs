using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement; 

public class LevelCompletion : MonoBehaviour
{
    //variables vor timer 
    private float time = 0.0f;
    private float currentTime = 0f;
    private float startingTime = 5f;

    //reference to countdown
    [SerializeField] private TextMeshProUGUI countdownText;

    //Reference to blocks 
    private GameObject buildingBlock;

    //reference to scene 
    [SerializeField] private string sceneToLoad;

    private void Start()
    {
        //Activate countdown text
        countdownText.enabled = false;

        //Set block
        buildingBlock = GameObject.FindGameObjectWithTag("BuildingBlock");
    }

    private void Update()
    {
        //Set text of countdown 
        countdownText.text = currentTime.ToString("0");
    }

    private void OnTriggerEnter(Collider other)
    {
        //If block enters trigger
        if (other.tag == "BuildingBlock")
        {
            //Set timer
            currentTime = startingTime;
            DragObject dragObjectScript = other.gameObject.GetComponent<DragObject>();  
            if(dragObjectScript.isOnTower == true)
            {
                Debug.Log("Colliding");
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //If block stays in trigger
        if (other.tag == "BuildingBlock")
        {
            //Check if colliding block stands on the tower 
            DragObject dragObjectScript = other.gameObject.GetComponent<DragObject>();
            if (dragObjectScript.isOnTower == true)
            {
                //start timer
                Debug.Log("Staying");
                countdownText.enabled = true;
                currentTime -= 1 * Time.deltaTime;

                //Load menu when timer is 0 meaning succes
                if (currentTime <= 0)
                {
                    currentTime = 0;
                    if (sceneToLoad == "StartMenu")
                    {
                        Cursor.lockState = CursorLockMode.None;
                    }
                    SceneManager.LoadScene(sceneToLoad);


                    Debug.Log("Success");
                }
            }
        }
    }

    
    private void OnTriggerExit(Collider other)
    {
        //If block exits trigger 
        if (other.tag == "BuildingBlock")
        {
            //Reset timer and countdown text 
            currentTime = startingTime;
            DragObject dragObjectScript = other.gameObject.GetComponent<DragObject>();
            if (dragObjectScript.isOnTower == true)
            {
                Debug.Log("Exiting");
                countdownText.enabled = false;
            }
        }
    }

}
