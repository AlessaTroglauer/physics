using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //reference to player character controller componenent 
    public CharacterController controller;

    //Adjustable variables 
    public float speed = 12f;
    public float gravity = -9.81f;

    // Varibales for groundcheck 
    public Transform groundCheck;
    public float groundDistance = 0.55f;
    public LayerMask groundMask; 

    private Vector3 velocity;
    private bool isGrounded; 

    void Update()
    {
        //Set if the player is grounded and adjust velocity
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask); 

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; 
        }

        //Set player input and transform it to movement 
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime); 
    }
}
