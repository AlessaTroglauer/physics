using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    //References to scenes
    [SerializeField] private string tutorial;
    [SerializeField] private string sceneOne;
    [SerializeField] private string sceneTwo;
    [SerializeField] private string sceneThree;

    //Load Different scenes
    public void LoadTutorial()
    {
        SceneManager.LoadScene(tutorial);
    }

    public void LoadLevel_01()
    {
        SceneManager.LoadScene(sceneOne);
    }

    public void LoadLevel_02()
    {
        SceneManager.LoadScene(sceneTwo);
    }

    public void LoadLevel_03()
    {
        SceneManager.LoadScene(sceneThree);
    }

    /*public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
    }
    */

    // Close the game 
    public void QuitGame()
    {
        Application.Quit();
    }
}
